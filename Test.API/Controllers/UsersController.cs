﻿using System;
using Test.DTO;
using Test.BLL.Interfaces;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Test.Api.Controllers
{
	[Route("api/Users")]
	[ApiController]
	public class UsersController : ControllerBase
	{
		private readonly IUserService _userService;
		public UsersController(IUserService userService)
		{
			_userService = userService;
		}

		// GET: api/Users
		[HttpGet]
		public async Task<IActionResult> Get()
		{
			var data = await _userService.GetAll();
			return Ok(data);
		}

		// GET: api/Users/5
		[HttpGet("{id}")]
		public async Task<IActionResult> Get(int id)
		{
			var data = await _userService.Get(id);
			if (data == null)
				return NotFound();
			return Ok(data);
		}

		[HttpPost]
		public async Task<IActionResult> Post([FromBody] UserDto model)
		{
			var data = await _userService.Add(model);
			return Created(new Uri($"{Request.Path}/{data.Id}", UriKind.Relative), data);
		}

		[HttpPost("{id}")]
		public async Task<IActionResult> Update(int id, [FromBody] UserDto model)
		{
			var data = await _userService.Get(id);
			if (data == null)
				return NotFound();

			model.Id = id;
			var result = await _userService.Update(model);
			return Ok(result);
		}

		[HttpDelete("{id}")]
		public async Task<IActionResult> Delete(int id)
		{
			var data = await _userService.Any(id);
			if (!data)
				return NotFound();

			await _userService.Remove(id);
			return NoContent();
		}
	}
}