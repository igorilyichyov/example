﻿using Test.DTO;
using Test.DAL.Entities;

namespace Test.BLL.Interfaces
{
    public interface IUserService:IServiceBase<User,UserDto>
    {
    }
}
