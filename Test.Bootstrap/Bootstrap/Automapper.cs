﻿using Test.DTO;
using AutoMapper;
using Test.DAL.Entities;
using Microsoft.Extensions.DependencyInjection;

namespace Test.Bootstrap.Bootstrap
{
	public static class Automapper
	{
		public static IServiceCollection ConfigureAutomapper(this IServiceCollection services)
		{
			services.AddAutoMapper(typeof(MappingProfile));

			return services;
		}
	}

	public class MappingProfile : Profile
	{
		public MappingProfile()
		{
			CreateMap<User, UserDto>().ReverseMap();
		}
	}
}