﻿namespace Test.Common
{
	public enum Status:int
    {
		None = 0,
		Pending = 1,
        Rejected = 2,
        Approved = 3
    }
}
