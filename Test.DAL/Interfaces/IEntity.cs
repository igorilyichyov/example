﻿namespace Test.DAL
{
	public interface IEntity
	{
		int Id { get; set; }
	}
}
