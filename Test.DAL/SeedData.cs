﻿using Test.Common;
using Test.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace Test.DAL
{
	public static class ModelBuilderExtention
	{
		public static void SeedData(this ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<User>().HasData(
			new User { Id = 1, Name = "User1" },
			new User { Id = 2, Name = "User2" });
		}
	}
}
