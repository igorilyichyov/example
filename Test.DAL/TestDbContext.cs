﻿using Test.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace Test.DAL
{
	public class TestDbContext : DbContext
	{
		public TestDbContext(DbContextOptions<TestDbContext> options) : base(options) { }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) { }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<User>().HasKey(x => x.Id);
			modelBuilder.SeedData();
		}

		public DbSet<User> Betshops { get; set; }
	}
}
